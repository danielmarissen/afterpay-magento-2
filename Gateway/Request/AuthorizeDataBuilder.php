<?php
/**
 * Copyright (c) 2019  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2019 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Afterpay\Payment\Model\Config\Advanced;
use Afterpay\Payment\Model\Config\Vat as VatConfig;
use Afterpay\Payment\Model\Config\Vat;
use Magento\Bundle\Model\Product\Price;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Backend\Model\Session\Quote as BackendCheckoutSession;
use Magento\Checkout\Model\Session as CheckoutSession;

class AuthorizeDataBuilder implements BuilderInterface
{
    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @var Vat
     */
    protected $vatConfig;

    /**
     * @var array
     */
    protected $result = [];

    /**
     * @var BackendCheckoutSession|CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var State
     */
    protected $appState;

    /**
     * @var Advanced
     */
    protected $advancedConfig;

    /**
     * AuthorizeDataBuilder constructor.
     *
     * @param SubjectReader $subjectReader
     * @param Vat $vatConfig
     * @param BackendCheckoutSession $backendSession
     * @param State $appState
     * @param CheckoutSession $checkoutSession
     * @param Advanced $advancedConfig
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        SubjectReader $subjectReader,
        VatConfig $vatConfig,
        BackendCheckoutSession $backendSession,
        State $appState,
        CheckoutSession $checkoutSession,
        Advanced $advancedConfig
    ) {
        $this->subjectReader = $subjectReader;
        $this->vatConfig = $vatConfig;
        $this->appState = $appState;
        # TODO checkoutSession must not be necessary here
        $this->checkoutSession =
            ($this->appState->getAreaCode() === Area::AREA_ADMINHTML) ? $backendSession : $checkoutSession;
        $this->advancedConfig = $advancedConfig;
    }

    /**
     * @param array $buildSubject
     *
     * @return array
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        /** @var Payment $payment */
        $payment = $paymentDO->getPayment();

        return $this->gatherAuthorizationData($payment);
    }

    /**
     * Prepare product lines
     *
     * @param Order $order
     */
    private function prepareProductLines(Order $order)
    {
        $quoteItems = $this->checkoutSession->getQuote()->getAllItems();
        /* @var Item $item */
        foreach ($quoteItems as $item) {
            if ($this->shouldBeSeparateLine($item)) {
                $vatAmount = $item->getTaxAmount();
                if ($item->getDiscountAmount() > 0.00 && !$this->advancedConfig->discountCalculationMode()) {
                    $vatAmount = $item->getBaseRowTotal() * $item->getTaxPercent() / 100;
                }
                if ($this->advancedConfig->discountCalculationMode()) {
                    $unitPrice = ($item->getRowTotalInclTax() - $item->getDiscountAmount()) * 100;
                } else {
                    $unitPrice = $item->getRowTotalInclTax() * 100;
                }
                $this->result['orderlines'][] = [
                    $item->getSku(),
                    $item->getQty() . ' x ' . $item->getName(),
                    '1',
                    (string) $unitPrice,
                    $this->vatConfig->getAfterpayVATCategory(
                        $order->getStoreId(),
                        $item->getTaxClassId()
                    ),
                    $vatAmount,
                    null,
                    null,
                    $item->getProduct()->getProductUrl(),
                    $item->getAfterpayProductImage()
                ];
            }
        }
    }

    /**
     * Determine whether quote item should be converted into order line. If item is not configurable child product or
     * bundle parent product with price 0.00 or dynamic price type, it will be added to order.
     *
     * @param Item $item
     *
     * @return bool
     */
    private function shouldBeSeparateLine(Item $item): bool
    {
        if ($parent = $item->getParentItem()) {
            if ($parent->getProduct()->getTypeId() !== Type::TYPE_BUNDLE) {
                return false;
            }
        } else {
            if ($item->getProduct()->getTypeId() === Type::TYPE_BUNDLE) {
                if ((int) $item->getProduct()->getPriceType() === Price::PRICE_TYPE_DYNAMIC
                    || $item->getProduct()->getFinalPrice() === 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Prepare discount line
     *
     * @param Order $order
     */
    private function prepareDiscountLine(Order $order)
    {
        // Check if discount calculation is enabled per items,
        // if it's enabled we skip rest of the logic and return early
        if ($this->advancedConfig->discountCalculationMode())
        {
            return;
        }
        $discountAmountItems = 0;
        $discountAmountCompensation = 0;
        $quoteItems = $order->getAllItems();
        foreach ($quoteItems as $item) {
            $discountAmountItems += $item->getDiscountAmount();
            $discountAmountCompensation += $item->getDiscountTaxCompensationAmount();
        }
        // discount is supposed to be negative
        $discountAmount = -1 * ($discountAmountItems * 100);
        // TODO: Implement a better way todo this
        if ($discountAmount !== 0 && $discountAmount !== -0.0) {
            $this->result['orderlines']['tempDiscount'] = [
                'DISCOUNT',
                'Discount',
                '1',
                (string) $discountAmount,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'discount'
                ),
                $discountAmountCompensation
            ];
        }
    }

    /**
     * Prepare shipping fee line
     *
     * @param OrderInterface $order
     */
    private function prepareShippingFeeLine(OrderInterface $order)
    {
        $shippingFee = $order->getShippingInclTax() * 100;
        if ($order->getShippingDiscountAmount() > 0.00) {
            $shippingFee = ($order->getShippingInclTax() - $order->getShippingDiscountAmount()) * 100;
        }
        if ($shippingFee !== 0) {
            $this->result['orderlines'][] = [

                'SHIPPING',
                'Shipping',
                '1',
                (string) $shippingFee,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'shipping'
                ),
                $order->getShippingTaxAmount()
            ];
        }
    }

    /**
     * Prepare payment fee line
     *
     * @param OrderInterface $order
     */
    private function preparePaymentFeeLine(OrderInterface $order)
    {
        $paymentFee = $order->getAfterpayPaymentFee() * 100;
        if ($paymentFee !== 0) {
            $this->result['orderlines'][] = [
                'FEE',
                'Payment Fee',
                '1',
                (string) $paymentFee,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'fee'
                )
            ];
        }
    }

    /**
     * Prepare payment fee line for Fooman Surcharge
     *
     * @param Order $order
     */
    private function prepareFoomanTotalLines(Order $order)
    {
        $extensionAttributes = $order->getExtensionAttributes();
        if (!$extensionAttributes) {
            return;
        }
        if (!method_exists($extensionAttributes, 'getFoomanTotalGroup')) {
            return;
        }
        $quoteAddressTotalGroup = $extensionAttributes->getFoomanTotalGroup();
        if (!$quoteAddressTotalGroup) {
            return;
        }
        $totals = $quoteAddressTotalGroup->getItems();
        if (empty($totals)) {
            return;
        }
        foreach ($totals as $total) {
            $paymentFee = ($total->getBaseAmount() + $total->getBaseTaxAmount()) * 100;
            $this->result['orderlines'][] = [
                'FEE',
                $total->getLabel(),
                '1',
                (string) $paymentFee,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'fee'
                ),
                $total->getTaxAmount()
            ];
        }
    }

    /**
     * Prepare reward point line
     *
     * @param OrderInterface $order
     */
    private function prepareStoreCreditLine(OrderInterface $order)
    {
        $storeCredit = $order->getBaseCustomerBalAmountUsed();
        if ($storeCredit) {
            $storeCredit = $storeCredit * 100 * -1;
            $this->result['orderlines'][] = [
                'STORCRED',
                'Store credits',
                '1',
                (string) $storeCredit,
                4
            ];
        }
    }

    /**
     * Prepare gift card line
     *
     * @param OrderInterface $order
     */
    private function prepareGiftCardLine(OrderInterface $order)
    {
        $giftCard = $order->getBaseGiftCardsAmount();
        if ($giftCard) {
            $giftCard = $giftCard * 100 * -1;
            $this->result['orderlines'][] = [
                'GIFTCARD',
                'Giftcard',
                '1',
                (string) $giftCard,
                4
            ];
        }
    }

    /**
     * Prepare reward point line
     *
     * @param OrderInterface $order
     */
    private function prepareRewardPointsLine(OrderInterface $order)
    {
        $rewardPoints = $order->getBaseRewardCurrencyAmount();
        if ($rewardPoints) {
            $rewardPoints = $rewardPoints * 100 * -1;
            $this->result['orderlines'][] = [
                'REWARD',
                'Reward Points',
                '1',
                (string) $rewardPoints,
                4
            ];
        }
    }

    /**
     * @param Payment $payment
     *
     * @return array
     */
    protected function gatherAuthorizationData(Payment $payment): array
    {
        $orderDO = $payment->getOrder();
        $this->prepareProductLines($orderDO);
        $this->prepareDiscountLine($orderDO);
        $this->preparePaymentFeeLine($orderDO);
        $this->prepareShippingFeeLine($orderDO);
        $this->prepareFoomanTotalLines($orderDO);
        $this->prepareGiftCardLine($orderDO);
        $this->prepareRewardPointsLine($orderDO);
        $this->prepareStoreCreditLine($orderDO);
        return $this->result;
    }
}
